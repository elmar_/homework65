import React, {useState, useEffect} from 'react';
import './Pages.css';
import axiosPage from "../../axios-page";

const Pages = props => {

    const [page, setPage] = useState({
        title: "",
        content: "",
        page: ""
    });
    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosPage.get('.json?orderBy="page"&equalTo="' + props.match.params.name + '"');
            const id = Object.keys(response.data);
            setPage(response.data[id[0]]);
        };
        fetchData().catch(console.error);
    }, [props.match.params.name]);

    let content = <p>Add something</p>;

    if (page) {
        content = (
            <div className="content">
                <h3>{page.title}</h3>
                <p>{page.content}</p>
            </div>
        );
    }

    return (
        <div className="Pages">
            <h2 className="title">Page: {props.match.params.name}</h2>
            {content}
        </div>
    );
};

export default Pages;