import React, {useState, useEffect, useRef} from 'react';
import PageName from "../../PAGENAME";
import './Admin.css';
import axiosPage from "../../axios-page";
import Spinner from "../../components/Spinner/Spinner";

const Admin = props => {

    const [data, setData] = useState({
        page: PageName[0],
        title: "",
        content: ""
    });

    const del = useRef('');
    const [loading, setLoading] = useState(false);

    const changeData = e => {
        const value = e.target.value;
        const name = e.target.name;

        setData({
            ...data,
            [name]: value
        });
    };

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            const response = await axiosPage.get('.json?orderBy="page"&equalTo="' + data.page + '"');
            if (response.status) {
                setLoading(false);
            }
            const id = Object.keys(response.data);
            if (id.length > 0) {
                del.current = id[0];
                setData(response.data[id[0]]);
            } else if (id.length === 0) {
                setData({
                    page: data.page,
                    title: "",
                    content: ""
                });
            }
        }
        fetchData().catch(console.error);
    }, [data.page]);

    const editData = async e => {
        e.preventDefault();
        if (del.current) {
            await axiosPage.delete(  del.current + '.json').catch(console.error);
        }
        await axiosPage.post('.json', data);
        props.history.push('/');
    };

    let form = (
        <form onSubmit={editData}>
            <h4 className="add">Edit pages</h4>
            <select value={data.page} onChange={changeData} name="page">
                {PageName.map(page => (
                    <option key={page}>{page}</option>
                ))}
            </select>
            <p>Title</p>
            <input type="text" name="title" value={data.title} onChange={changeData} />
            <p>Content</p>
            <textarea name="content" value={data.content} onChange={changeData} />
            <button>Edit</button>
        </form>
    );

    if (loading) {
        form = <Spinner />
    }
    return (
        <div className="Admin">
            {form}
        </div>
    );
};

export default Admin;