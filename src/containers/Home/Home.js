import React from 'react';
import './Home.css';

const Home = () => {
    return (
        <div className="Home">
            <h2 className="title">Page: Home</h2>
            <p>This is home</p>
        </div>
    );
};

export default Home;