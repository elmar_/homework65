import React from 'react';
import {NavLink} from "react-router-dom";
import PageName from "../../PAGENAME";
import './Toolbat.css';

const Toolbar = () => {
    return (
        <div className="Toolbar">
            <a href="/" className="Logo">Logo</a>
            <nav>
                <ul>
                    <li>
                        <NavLink to="/">home</NavLink>
                    </li>
                    {PageName.map(page => (
                        <li key={page}>
                            <NavLink to={"/pages/" + page}>{page}</NavLink>
                        </li>
                    ))}
                    <li>
                        <NavLink to="/admin">admin</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default Toolbar;