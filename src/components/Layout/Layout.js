import React from 'react';
import './Layout.css';
import Toolbar from "../Toolbar/Toolbar";

const Layout = props => {
    return (
        <div className="Layout">
            <Toolbar />
            {props.children}
        </div>
    );
};

export default Layout;