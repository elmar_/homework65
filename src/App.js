import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./containers/Home/Home";
import Layout from "./components/Layout/Layout";
import Pages from "./containers/Pages/Pages";
import Admin from "./containers/Admin/Admin";

const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/pages/:name" component={Pages} />
                <Route path="/admin" component={Admin} />
            </Switch>
        </Layout>
    </BrowserRouter>
);

export default App;
