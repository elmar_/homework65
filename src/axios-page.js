import axios from "axios";

const axiosPage = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/pages'
});
export default axiosPage;